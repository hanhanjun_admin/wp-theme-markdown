# wp-theme-markdown

#### 项目介绍
该项目可以吧markdown 编辑器集成到主题里去

#### 使用说明

- 将文件夹放在当前主题目录里

- 在当前主题目录找到 ```functions.php```

- 调用插件

```php
// 'wp-editormd/wp-editormd'   插件目录/wp-editormd.php
get_template_part('wp-editormd/wp-editormd');
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)